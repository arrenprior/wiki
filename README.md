# Wikipedia REST Tools

A REST service with a couple of endpoints enabling some interesting processing of Wikipedia articles for fun. The project is written using Java and the Spring Boot framework.

## Random Article

The name of a random Wikipedia article can be retrieved by sending a GET request to the following endpoint:

`localhost:8080/wiki/random`

## Minimum Number of Clicks

The minimum number of clicks required to get from one article to another using links in each article traversed can be retrieved using a GET request to the following endpoint:

`localhost:8080/wiki/<starting_article_name>/<end_article_name>`

