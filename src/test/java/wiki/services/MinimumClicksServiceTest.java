package wiki.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import wiki.exceptions.ImpossibleToReachArticleException;
import wiki.exceptions.SearchDepthLimitReachedException;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static wiki.services.ArticleService.WIKI_URL;

@ExtendWith(SpringExtension.class)
@Import(MinimumClicksService.class)
public class MinimumClicksServiceTest {
    @Autowired
    private MinimumClicksService minimumClicksService;

    @MockBean
    private ArticleService articleService;

    @Test
    public void testFindMinimumClicksFoundStartArticle() {
        // Arrange
        String startArticle = "A";
        String endArticle = "B";
        when(articleService.findLinksInArticle(startArticle)).thenReturn(Set.of(endArticle));

        // Act
        int result = minimumClicksService.findMinimumClicks(startArticle, endArticle);

        // Assert
        assert result == 1;
    }

    @Test
    public void testFindMinimumClicksFoundRecursive() {
        // Arrange
        String startArticle = "A";
        String connectingArticle = "B";
        String endArticle = "C";
        when(articleService.findLinksInArticle(startArticle)).thenReturn(Set.of(connectingArticle));
        when(articleService.findLinksInArticle(connectingArticle)).thenReturn(Set.of(endArticle));

        // Act
        int result = minimumClicksService.findMinimumClicks(startArticle, endArticle);

        // Assert
        assert result == 2;
    }

    @Test
    public void testFindMinimumClicksImpossible() {
        // Arrange
        String startArticle = "A";
        String connectingArticle = "B";
        String endArticle = "C";
        when(articleService.findLinksInArticle(startArticle)).thenReturn(Set.of(connectingArticle));
        when(articleService.findLinksInArticle(connectingArticle)).thenReturn(Set.of());

        // Act & Assert
        Exception e = assertThrows(ImpossibleToReachArticleException.class, () ->
                minimumClicksService.findMinimumClicks(startArticle, endArticle));

        assert e.getMessage().equals(String.format("It is impossible to click from %s%s to %s%s", WIKI_URL, startArticle, WIKI_URL, endArticle));
    }

    @Test
    public void testFindMinimumClicksTooDeep() {
        // Arrange
        String startArticle = "A";
        String connectingArticle1 = "B";
        String connectingArticle2 = "C";
        String connectingArticle3 = "D";
        String connectingArticle4 = "E";
        String connectingArticle5 = "F";
        String connectingArticle6 = "G";
        String connectingArticle7 = "H";
        String connectingArticle8 = "I";
        String connectingArticle9 = "J";
        String connectingArticle10 = "K";
        String endArticle = "L";

        when(articleService.findLinksInArticle(startArticle)).thenReturn(Set.of(connectingArticle1));
        when(articleService.findLinksInArticle(connectingArticle1)).thenReturn(Set.of(connectingArticle2));
        when(articleService.findLinksInArticle(connectingArticle2)).thenReturn(Set.of(connectingArticle3));
        when(articleService.findLinksInArticle(connectingArticle3)).thenReturn(Set.of(connectingArticle4));
        when(articleService.findLinksInArticle(connectingArticle4)).thenReturn(Set.of(connectingArticle5));
        when(articleService.findLinksInArticle(connectingArticle5)).thenReturn(Set.of(connectingArticle6));
        when(articleService.findLinksInArticle(connectingArticle6)).thenReturn(Set.of(connectingArticle7));
        when(articleService.findLinksInArticle(connectingArticle7)).thenReturn(Set.of(connectingArticle8));
        when(articleService.findLinksInArticle(connectingArticle8)).thenReturn(Set.of(connectingArticle9));
        when(articleService.findLinksInArticle(connectingArticle9)).thenReturn(Set.of(connectingArticle10));
        when(articleService.findLinksInArticle(connectingArticle10)).thenReturn(Set.of(endArticle));

        // Act & Assert
        Exception e = assertThrows(SearchDepthLimitReachedException.class, () ->
                minimumClicksService.findMinimumClicks(startArticle, endArticle));

        assert e.getMessage().equals(String.format("Could not find path from %s%s to %s%s before reaching %d click limit", WIKI_URL, startArticle, WIKI_URL, endArticle, 5));
    }
}
