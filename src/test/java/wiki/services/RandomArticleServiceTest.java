package wiki.services;

import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@Import(RandomArticleService.class)
public class RandomArticleServiceTest {
    @Autowired
    private RandomArticleService randomArticleService;

    @MockBean
    private ArticleService articleService;

    @Test
    public void testGetRandomArticle() {
        // Arrange
        String article = "article";
        Document mockDocument = mock(Document.class);
        when(articleService.getArticle("Special:Random")).thenReturn(mockDocument);
        when(articleService.getArticleName(mockDocument)).thenReturn(article);

        // Act
        String result = randomArticleService.getRandomArticle();

        // Assert
        assert result.equals(article);
    }

}
