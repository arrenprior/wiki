package wiki.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import wiki.utils.WebDownloader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.stream.Collectors;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static wiki.services.ArticleService.WIKI_URL;

@ExtendWith(SpringExtension.class)
@Import(ArticleService.class)
public class ArticleServiceTest {
    @Autowired
    private ArticleService articleService;

    @MockBean
    WebDownloader webDownloader;

    @Test
    public void testGetArticle() throws IOException {
        // Arrange
        String article = "article";
        Document mockDocument = mock(Document.class);
        when(webDownloader.download(String.format("%s%s", WIKI_URL, article))).thenReturn(mockDocument);

        // Act
        Document result = articleService.getArticle(article);

        // Assert
        assert result == mockDocument;
    }

    @Test
    public void testGetArticleName() throws IOException {
        // Arrange
        String article = "article";
        String html = getResourceFileAsString("zychowo.html");
        Document document = Jsoup.parse(html);

        // Act
        String result = articleService.getArticleName(document);

        // Assert
        assert result.equals("Żychowo");
    }

    @Test
    public void testFindLinksInArticle() throws IOException {
        // Arrange
        String article = "article";
        String html = getResourceFileAsString("zychowo.html");
        Document document = Jsoup.parse(html);
        when(webDownloader.download(String.format("%s%s", WIKI_URL, article))).thenReturn(document);

        // Act
        Set<String> result = articleService.findLinksInArticle(article);

        // Assert
        assert result.size() == 11;
    }

    private String getResourceFileAsString(String fileName) throws IOException {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        try (InputStream is = classLoader.getResourceAsStream(fileName)) {
            if (is == null) return null;
            try (InputStreamReader isr = new InputStreamReader(is);
                 BufferedReader reader = new BufferedReader(isr)) {
                return reader.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        }
    }
}
