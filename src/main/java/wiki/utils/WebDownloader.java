package wiki.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class WebDownloader {
    public Document download(String url) throws IOException {
        return Jsoup.connect(url).get();
    }
}
