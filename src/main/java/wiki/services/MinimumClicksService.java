package wiki.services;

import org.springframework.stereotype.Service;
import wiki.exceptions.ImpossibleToReachArticleException;
import wiki.exceptions.SearchDepthLimitReachedException;

import java.util.*;
import java.util.stream.Collectors;

import static wiki.services.ArticleService.WIKI_URL;


@Service
public class MinimumClicksService {
    private static final int DEPTH_LIMIT = 5; // It starts to take a very long time after this...

    private final ArticleService articleService;

    public MinimumClicksService(ArticleService articleService) {
        this.articleService = articleService;
    }

    /**
     * Find the minimum number of clicks required to navigate from one Wikipedia article to another.
     *
     * @param articleStart The article from which to start the search.
     * @param articleEnd The article at which the search will finish.
     * @return The minimum number of clicks required to navigate between the articles.
     */
    public int findMinimumClicks(String articleStart, String articleEnd) {
        return findMinimumClicks(articleStart, articleEnd, 0, new HashSet<>(Collections.singletonList(articleStart)), new HashSet<>());
    }

    private int findMinimumClicks(String articleStart, String articleEnd, int clicks, Set<String> currentArticles, Set<String> articlesSoFar) {
        clicks++;
        if (clicks > DEPTH_LIMIT) {
            throw new SearchDepthLimitReachedException(String.format("Could not find path from %s%s to %s%s before reaching %d click limit", WIKI_URL, articleStart, WIKI_URL, articleEnd, DEPTH_LIMIT));
        }

        // Find all links in articles at current depth, filter out those we've seen before to avoid loops.
        Set<String> newArticles = currentArticles.stream()
                .flatMap(article -> articleService.findLinksInArticle(article).stream())
                .filter(article -> !articlesSoFar.contains(article))
                .collect(Collectors.toSet());

        if (newArticles.isEmpty()) {
            throw new ImpossibleToReachArticleException(String.format("It is impossible to click from %s%s to %s%s", WIKI_URL, articleStart, WIKI_URL, articleEnd));
        } else if (newArticles.contains(articleEnd)) {
            return clicks;
        }

        articlesSoFar.addAll(newArticles);

        return findMinimumClicks(articleStart, articleEnd, clicks, newArticles, articlesSoFar);
    }


}
