package wiki.services;

import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Service
public class RandomArticleService {
    private final ArticleService articleService;

    public RandomArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    /**
     * Returns the name of a random Wikipedia article.
     *
     * @return The name of a random Wikipedia article.
     */
    public String getRandomArticle() {
        Document article = articleService.getArticle("Special:Random");
        return articleService.getArticleName(article);
    }

}
