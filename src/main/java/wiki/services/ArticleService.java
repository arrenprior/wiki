package wiki.services;

import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;
import wiki.exceptions.WikipediaArticleDownloadException;
import wiki.utils.WebDownloader;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ArticleService {
    public static final String WIKI_URL = "https://en.wikipedia.org/wiki/";

    private final WebDownloader webDownloader;

    public ArticleService(WebDownloader webDownloader) {
        this.webDownloader = webDownloader;
    }

    public Document getArticle(String articleName) {
        try {
            return webDownloader.download(String.format("%s%s", WIKI_URL, articleName));
        } catch (IOException e) {
            throw new WikipediaArticleDownloadException(String.format("Exception occurred when attempting downloading the Wikipedia article '%s': %s", articleName, e.toString()));
        }
    }

    public String getArticleName(Document article) {
        return article.body()
                .getElementById("content")
                .getElementById("firstHeading")
                .text();
    }

    public Set<String> findLinksInArticle(String articleName) {
            Document article = getArticle(articleName);
            return article.body()
                    .getElementById("content")
                    .getElementById("bodyContent")
                    .getElementById("mw-content-text")
                    .getElementsByClass("mw-parser-output")
                    .get(0)
                    .getElementsByTag("p")
                    .stream()
                    .flatMap(p -> p.getElementsByTag("a").stream())
                    .map(a -> a.attr("href"))
                    .filter(href -> href.startsWith("/wiki/"))
                    .map(link -> link.substring(6))
                    .collect(Collectors.toSet());
    }
}
