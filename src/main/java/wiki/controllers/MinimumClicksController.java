package wiki.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wiki.services.MinimumClicksService;

import static wiki.services.ArticleService.WIKI_URL;

@RestController
@RequestMapping("/wiki")
public class MinimumClicksController {
    private final MinimumClicksService minimumClicksService;

    @Autowired
    public MinimumClicksController(MinimumClicksService minimumClicksService) {
        this.minimumClicksService = minimumClicksService;
    }

    @GetMapping("/minimum/{start}/{end}")
    String findMinimumClicks(@PathVariable String start, @PathVariable String end) {
        int clicks = minimumClicksService.findMinimumClicks(start, end);
        return String.format("There are %d clicks required to click from %s%s to %s%s", clicks, WIKI_URL, start, WIKI_URL, end);
    }
}
