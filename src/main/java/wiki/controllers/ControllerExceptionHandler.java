package wiki.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import wiki.exceptions.ImpossibleToReachArticleException;
import wiki.exceptions.SearchDepthLimitReachedException;
import wiki.exceptions.WikipediaArticleDownloadException;


@ControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler({SearchDepthLimitReachedException.class, ImpossibleToReachArticleException.class})
    public ResponseEntity<String> handle(RuntimeException e) {
        return ResponseEntity.status(422).body(e.getMessage());
    }

    @ExceptionHandler()
    public ResponseEntity<String> handle(WikipediaArticleDownloadException e) {
        return ResponseEntity.status(502).body(e.getMessage());
    }
}
