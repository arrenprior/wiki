package wiki.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wiki.services.RandomArticleService;

@RestController
@RequestMapping("/wiki")
public class RandomArticleController {
    private final RandomArticleService randomArticleService;

    public RandomArticleController(RandomArticleService randomArticleService) {
        this.randomArticleService = randomArticleService;
    }

    @GetMapping("/random")
    String getRandomArticle() {
        return randomArticleService.getRandomArticle();
    }
}
