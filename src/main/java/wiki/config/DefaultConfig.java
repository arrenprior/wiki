package wiki.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import wiki.utils.WebDownloader;

@Configuration
@Profile("default")
public class DefaultConfig {
    @Bean
    public WebDownloader webPageDownloader() {
        return new WebDownloader();
    }
}
