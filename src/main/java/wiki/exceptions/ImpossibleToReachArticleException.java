package wiki.exceptions;

public class ImpossibleToReachArticleException extends RuntimeException {
    public ImpossibleToReachArticleException(String message) {
        super(message);
    }
}
