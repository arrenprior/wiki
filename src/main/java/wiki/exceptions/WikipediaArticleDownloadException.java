package wiki.exceptions;

public class WikipediaArticleDownloadException extends RuntimeException {
    public WikipediaArticleDownloadException(String message) {
        super(message);
    }
}
