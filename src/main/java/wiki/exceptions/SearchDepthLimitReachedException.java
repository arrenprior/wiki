package wiki.exceptions;

public class SearchDepthLimitReachedException extends RuntimeException {
    public SearchDepthLimitReachedException(String message) {
        super(message);
    }
}
